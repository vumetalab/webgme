/*jshint node: true*/
/**
 * @author lattmann / https://github.com/lattmann
 * @author pmeijer / https://github.com/pmeijer
 */


var path = require('path'),
    config = require('webgme-engine/config/config.default');


// Overwrite the appDir from webgme-engine (it only provides a dummy app).
config.client.appDir = path.join(__dirname, '../src/client');

// These client options are added by the webgme app
config.client.appVersion = require(path.join(__dirname, '../package.json')).version;
config.client.defaultConnectionRouter = 'basic3'; //'basic', 'basic2', 'basic3'
config.client.errorReporting = {
                enable: false,
                DSN: '',
                // see https://docs.sentry.io/clients/javascript/config/
                ravenOptions: null // defaults to {release: <webgme-version>}
            };

config.client.allowUserDefinedSVG = true;
config.visualization.extraCss = [];

// The webgme-engine does not populate any of these
config.visualization.svgDirs = [path.join(__dirname, '../src/client/assets/DecoratorSVG')];
config.visualization.decoratorPaths = [path.join(__dirname, '../src/client/decorators')];
config.visualization.visualizerDescriptors = [path.join(__dirname, '../src/client/js/Visualizers.json')];
config.visualization.panelPaths = [path.join(__dirname, '../src/client/js/Panels')];
config.visualization.layout.basePaths = [path.join(__dirname, '../src/client/js/Layouts')];

config.plugin.basePaths.push( path.join(__dirname, '../src/plugins') );

// config.storage.keyType = 'rustSHA1'; //check it out
module.exports = config;


// authentication
//
config.authentication.enable = true
config.authentication.allowGuests = true
config.authentication.guestAccount = 'guest'
config.authentication.allowUserRegistration = true
config.authentication.registeredUsersCanCreate = true
// config.authentication.inferredUsersCanCreate = false
config.authentication.logInUrl = '/profile/login'
config.authentication.logOutUrl = '/profile/login'
// config.authentication.userManagementPage = 'webgme-user-management-page'
config.authentication.userManagementPage = require.resolve('webgme-user-management-page');
// config.authentication.salts = 10
// config.authentication.authorizer.path = 'node_modules/webgme-engine/src/server/middleware/auth/defaultauthorizer'
// config.authentication.authorizer.options = {}
// config.authentication.jwt.cookieId = 'access_token'
// config.authentication.jwt.expiresIn = 3600 * 24 * 7
// config.authentication.jwt.renewBeforeExpires = 3600
config.authentication.jwt.privateKey = path.join(__dirname, '..', '..', 'token_keys', 'private_key');
// 'node_modules/webgme-engine/src/server/middleware/auth/EXAMPLE_PRIVATE_KEY'
config.authentication.jwt.publicKey = path.join(__dirname, '..', '..', 'token_keys', 'public_key');
// 'node_modules/webgme-engine/src/server/middleware/auth/EXAMPLE_PRIVATE_KEY'
// config.authentication.jwt.algorithm = 'RS256'
// config.authentication.jwt.tokenGenerator = 'node_modules/webgme-engine/src/server/middleware/auth/localtokengenerator.js'
// config.authentication.adminAccount = null
// config.authentication.publicOrganizations = []



config.server.port = 3000
// config.server.handle = null
// config.server.timeout = -1 
// config.server.maxWorkers = 10
// config.server.maxQueuedWorkerRequests = -1
// config.server.workerDisconnectTimeout = 2000
// config.server.workerManager.path = 'node_modules/webgme-engine/src/server/worker/serverworkermanager'
// config.server.workerManager.options = {}
// config.server.log = see webgme-engine default.config
// config.server.extlibExcludes = ['.\.pem$', 'config\/config\..*\.js$']
// config.server.behindSecureProxy = false

